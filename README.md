The core [image](https://sourceforge.net/projects/mecrisp/files/mecrisp-stellaris-2.4.0.tar.gz/download) is stm32f103-ra with disassebmler: __mecrisp-stellaris-2.4.0/stm32f103-ra/mecrisp-stellaris-stm32f103-with-disassembler-m3.bin__

``` sh
 ./buildcore-stm ./thumbulator-stm32f103 ../stm32f103-ra/da.bin ../stm32f103-ra/display.txt ../stm32f103-ra/disp.bin
```

Board wiring 
PA9/10 - Serial console
PB6/7 - I2C to pcf8574 and bme280

1. SCL - PB6
2. SDA - PB7
