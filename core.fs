\ cornerstone <<<display>>>

: INIT ( -- )
    ." Display" cr ;

include cond.fs
include hexdump.fs
include io.fs
include pins48.fs
include hal.fs

$10 constant I2C.DELAY 
include i2c-bb.fs
include lcd.fs


