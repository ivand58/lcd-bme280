\ cornerstone <<<display>>>

: INIT ( -- )
    ." BME280" cr ;

include cond.fs
include hexdump.fs
include io.fs
include pins48.fs
include hal.fs
include ring.fs
include i2c.fs
include lcd.fs
include b1.fs

: INIT ( -- )
	INIT 
	OMODE-OD PC13 io-mode!
    #1000 systick-hz
    200 ms 
    i2c-init
    400 ms 
	i2c.
	$42000 0 do loop
	2 lcd-init
	$42000 0 do loop
    200 ms 
	lcd" BME280"
    1000 ms 
    bme-init drop
    200 ms 
    bme-calib \ params 32 dump
    bme-data bme-calc drop drop drop
	main 
    ;
